// Constructeurs

// Constructeur d'une personne
function Personne() {
    this.lastNames = null;
    this.firstNames = null; // peut être composé avec un ou plusieurs traits d'union mais peut aussi être plusieurs prénoms à la suite dans un même string
    this.groups = null; // les groupes auxquelles la personne appartient en dehors de l'événement (que ça soit un service, un groupe affinitaire ou autre)
    this.email = null;
    this.phone = null;
    this.partOfOrg = false;
    this.presence = {
        "AM1" : false,
        "PM1" : false,
        "AM2" : false,
        "PM2" : false
    };
    this.speedDatesByRound = {
        1: null,
        2: null,
        3: null,
        4: null
    };
    this.assignedToWorkshop = false;
}

// Constructeur d'un atelier
function Atelier() {
    this.number;
    this.theme;
    this.name;
    this.members = [];
}

// Constructeur d'un thème
function Theme() {
    this.name;
    this.occurrence = 0;
    this.isMostPrefered = false;
    this.isLeastPrefered = false;
}

// Functions par sections

// Import-export

// Export

let jsonExportButton = document.querySelector('#json_export_button');
jsonExportButton.onclick = jsonExport;
let jsonExportLink = document.querySelector('#json_export_link');

function jsonExport() {
    let fullToExport = { // This is a JS object (like a dictionnary).
        "eventName": eventName,
        "invitees": invitees 
    }

    let jsonedFullToExport = JSON.stringify(fullToExport) // It turns it into Json object literal.
    let filedFullToExport = new File([jsonedFullToExport], eventName.trim() + "_export.json"); // requiere [] around the string gave by JSON.stringify, I don't know why. It turns it into a file
    let urledFullToExport = URL.createObjectURL(filedFullToExport); // It turns into an URL 
    jsonExportLink.href = urledFullToExport; // It gives the URL to the link to the <a download> that embeds the button with the function so that the button will offer to download what is at this local URL (I guess it means that the href of the a is checked after the function onclick associated with the button is resolved, but i don't know why).
}

// Import

let jsonImportButton = document.querySelector('#json_import_button');
jsonImportButton.onclick = jsonImport;

function jsonImport() {
    let jsonImportFileSelector = document.querySelector('#json_import_file_selector');
    let filedInvitees = jsonImportFileSelector.files[0]; // Select the first file gave to the "parcourir" input.

    let fileReader = new FileReader(); // This is a geenric tool.
    fileReader.onload = function() { // This tells the tool that when it will later and async read successfully a file, it will have to do the given function.
        let jsonedFullToImport = JSON.parse(fileReader.result);
        // import eventName
        eventName = jsonedFullToImport.eventName;
        // display eventName
        eventNameDisplay.textContent = eventName;
        documentTitle.textContent = eventName + " - Gestion des participant.e.s";
        // import invitees 
        invitees = jsonedFullToImport.invitees;
        // display invitees
        updateInviteesTable();
        updatePresenceTable();
        computingSpeedDatingInfo();
    };

    fileReader.readAsText(filedInvitees); // Actually reads the file and will trigger onload when done reading (back-office)
}


// Nom de l'événement

let eventName = "Evenement anonyme";
let documentTitle = document.querySelector('title');
let eventNameDisplay = document.querySelector('#event_name_display');
let eventNameInput = document.querySelector('#event_name_input');

let eventNameModifyButton = document.querySelector('#event_name_modify_button');
eventNameModifyButton.onclick = modifyEvenement;

function modifyEvenement() {
    eventName = eventNameInput.value;
    eventNameDisplay.textContent = eventName;
    documentTitle.textContent = eventName + " - Gestion des participant.e.s";
}

// Les invité.e.s

let invitees = [];

let inviteesTable = document.querySelector('#invitees_table'); // This table focuses on displaying most of the invitees info
let inviteesTableBody = document.querySelector("#invitees_table_body");
let inviteesTableAddInviteeRow = document.querySelector("#invitees_table_add_invitee_row");

// Ajouter un.e invité.e
function addInvitee() {
    let newInviteeLastNamesInput = document.querySelector('#new_invitee_last_names').value;
    let newInviteeFirstNamesInput = document.querySelector('#new_invitee_first_names').value;
    let newInviteeGroupsInput = document.querySelector('#new_invitee_groups').value;
    let newinviteeEmailInput = document.querySelector('#new_invitee_email').value;
    let newInviteePhoneInput = document.querySelector('#new_invitee_phone').value;
    let newInviteePartOfOrgInput = document.querySelector('#new_invitee_part_of_org').value;

    newInvitee = new Personne();

    if (newInviteeLastNamesInput != "")
    {
        newInvitee.lastNames = newInviteeLastNamesInput;
    }
    
    if (newInviteeFirstNamesInput != "")
    {
        newInvitee.firstNames = newInviteeFirstNamesInput;
    }

    if (newInviteeGroupsInput != "")
    {
        newInvitee.groups = [];

        newInviteeGroupsSplitted = newInviteeGroupsInput.split(',');

        newInviteeGroupsSplitted.forEach(group => {
            group = group.trim();
            let casedGroup = "";
            for (let char = 0; char < group.length; char++)
            {
                casedGroup += group[char].toUpperCase();
            }
            // group = group[0].toUpperCase() + group.slice(1); // Pour que la première lettre au moins soit en majuscule
            newInvitee.groups.push(casedGroup);
        });
    }
    
    if (newinviteeEmailInput != ""){
        newInvitee.email = newinviteeEmailInput;
    }
    if (newInviteePhoneInput != ""){
        newInvitee.phone = newInviteePhoneInput;
    }

    if (newInviteePartOfOrgInput == 'oui' || newInviteePartOfOrgInput == 'Oui')
    {
        newInvitee.partOfOrg = true;
    }

    invitees.push(newInvitee);

    

    // Modifier l'affichage dans les tableaux
    updateInviteesTable();
    updatePresenceTable();
    computingSpeedDatingInfo();

    // Reset les inputs utilisateurs
    document.querySelector('#new_invitee_last_names').value = null;
    document.querySelector('#new_invitee_first_names').value = null;
    document.querySelector('#new_invitee_groups').value = null;
    document.querySelector('#new_invitee_email').value = null;
    document.querySelector('#new_invitee_phone').value = null;
    document.querySelector('#new_invitee_part_of_org').value = null;
}

// Mettre à jour l'affichage (tableau) des invité.e.s
function updateInviteesTable() {
    // Indiquer qu'il n'y a plus de mis à jour en cours (pour permettre une nouvelle action de l'user, voir plus bas)
    alreadyAnInviteeBeingModified = false;

    // Effacer le centre de la table qui doit être mis à jour
    while (inviteesTableBody.childElementCount > 1) // Car il faut conserver le dernier rang, celui qui permet d'ajouter de nouvelleaux invité.e.s
    {
        inviteesTableBody.removeChild(inviteesTableBody.children[0]);
    }

    // Créer de nouveau le centre de la table
    invitees.forEach(invitee => {
        
        // La div juste pour une personne
        let inviteeRow = document.createElement("tr");
        inviteeRow.className = "inner_row";
        
        let inviteeLastNamesCell = document.createElement('td');
        inviteeLastNamesCell.className = "styled_td";
        if (invitee.lastNames == null){
            inviteeLastNamesCell.textContent = "";
        }
        else{
            inviteeLastNamesCell.textContent = invitee.lastNames;
        }        
        inviteeRow.appendChild(inviteeLastNamesCell);

        let inviteeFirstNamesCell = document.createElement('td');
        inviteeFirstNamesCell.className = "styled_td";

        if (invitee.firstNames == null){
            inviteeFirstNamesCell.textContent = "";
        }
        else{
            inviteeFirstNamesCell.textContent = invitee.firstNames;
        }
        inviteeRow.appendChild(inviteeFirstNamesCell);      

        let inviteeGroupsCell = document.createElement('td');
        inviteeGroupsCell.className = "styled_td";

        if (invitee.groups == null){
            inviteeGroupsCell.textContent = "";
        }
        else{
            inviteeGroupsCell.textContent = invitee.groups[0];
            for (let group = 1; group < invitee.groups.length; group++){
                inviteeGroupsCell.textContent += ", " + invitee.groups[group];     
            };
        }
        inviteeRow.appendChild(inviteeGroupsCell);

        let inviteeEmailCell = document.createElement('td');
        inviteeEmailCell.className = "styled_td";

        if (invitee.email == null){
            inviteeEmailCell.textContent = "";
        }
        else{
            inviteeEmailCell.textContent = invitee.email;
        }
        inviteeRow.appendChild(inviteeEmailCell);

        let inviteePhoneCell = document.createElement('td');
        inviteePhoneCell.className = "styled_td";

        if (invitee.phone == null){
            inviteePhoneCell.textContent = "";
        }
        else{
            inviteePhoneCell.textContent = invitee.phone;
        }
        inviteeRow.appendChild(inviteePhoneCell);

        let inviteePartOfOrgCell = document.createElement('td');
        inviteePartOfOrgCell.className = "styled_td";

        if (invitee.partOfOrg == true)
        {
            inviteePartOfOrgCell.textContent = "Oui";
        }
        else 
        {
            inviteePartOfOrgCell.textContent = "";
        }
        inviteeRow.appendChild(inviteePartOfOrgCell);

        let inviteeButtonCell = document.createElement('td');
        inviteeButtonCell.className = "invitees_list_buttons_cell";
        let inviteeModButton = document.createElement('input');
        inviteeModButton.type = "button";
        inviteeModButton.value = "Modifier";
        inviteeModButton.onclick = function() {
            if(alreadyAnInviteeBeingModified == false)
            {
                switchToMod(inviteeRow, invitee)
            }
        };
        inviteeButtonCell.appendChild(inviteeModButton);
        let inviteeDelButton = document.createElement('input');
        inviteeDelButton.type = "button";
        inviteeDelButton.value= "Supprimer"
        inviteeDelButton.onclick = function() {delInvitee(invitee)};
        inviteeButtonCell.appendChild(inviteeDelButton);
        inviteeRow.appendChild(inviteeButtonCell);

        inviteesTableBody.insertBefore(inviteeRow, inviteesTableAddInviteeRow);        
    });
}

// Modifier un.e invité.e

let alreadyAnInviteeBeingModified = false;
function switchToMod(inviteeRow, invitee)
{
    alreadyAnInviteeBeingModified = true;
    inviteeRow.innerHTML = "";
    
    let tD0 = document.createElement('td');
    let inviteeModLastNamesInput = document.createElement('input');
    inviteeModLastNamesInput.className = "invitee_mod_last_names_input";
    inviteeModLastNamesInput.type = "text";
    inviteeModLastNamesInput.placeholder = "Nom.s de famille";
    inviteeModLastNamesInput.value = invitee.lastNames;
    tD0.appendChild(inviteeModLastNamesInput);
    inviteeRow.appendChild(tD0);

    let tD1 = document.createElement('td');
    let inviteeModFirstNamesInput = document.createElement('input');
    inviteeModFirstNamesInput.className = "invitee_mod_first_names_input";
    inviteeModFirstNamesInput.type = "text";
    inviteeModFirstNamesInput.placeholder = "Prénom.s";
    inviteeModFirstNamesInput.value = invitee.firstNames;
    tD1.appendChild(inviteeModFirstNamesInput);
    inviteeRow.appendChild(tD1);

    let tD2 = document.createElement('td');
    let inviteeModGroupsInput = document.createElement('input');
    inviteeModGroupsInput.className = "invitee_mod_groups_input";
    inviteeModGroupsInput.type = "text";
    inviteeModGroupsInput.placeholder = "Séparés par une virgule";
    // Just for group temp display in input so that the soft know if the user ched it or not.
    if (invitee.groups == null){
        inviteeModGroupsInput.value = "";
    }
    else{
        inviteeModGroupsInput.value = invitee.groups[0];
        for (let group = 1; group < invitee.groups.length; group++){
            inviteeModGroupsInput.value += ", " + invitee.groups[group];     
        };
    }
    tD2.appendChild(inviteeModGroupsInput);
    inviteeRow.appendChild(tD2);

    let tD3 = document.createElement('td');
    let inviteeModEmailInput = document.createElement('input');
    inviteeModEmailInput.className = "invitee_mod_email_input";
    inviteeModEmailInput.type = "text";
    inviteeModEmailInput.placeholder = "L'adresse de contact";
    inviteeModEmailInput.value = invitee.email;
    tD3.appendChild(inviteeModEmailInput);
    inviteeRow.appendChild(tD3);

    let tD4 = document.createElement('td');
    let inviteeModPhoneInput = document.createElement('input');
    inviteeModPhoneInput.className = "invitee_mod_phone_input";
    inviteeModPhoneInput.type = "text";
    inviteeModPhoneInput.placeholder = "Numéro de téléphone";
    inviteeModPhoneInput.value = invitee.phone;
    tD4.appendChild(inviteeModPhoneInput);
    inviteeRow.appendChild(tD4);
    
    let tD5 = document.createElement('td');
    let inviteeModPartOfOrgInput = document.createElement('input');
    inviteeModPartOfOrgInput.className = "invitee_mod_part_of_org_input";
    inviteeModPartOfOrgInput.type = "text";
    inviteeModPartOfOrgInput.placeholder = "Indiquer 'oui' ou rien";
    if (invitee.partOfOrg == true) {
        inviteeModPartOfOrgInput.value = "Oui";
    }
    else if (invitee.partOfOrg == false) {
        inviteeModPartOfOrgInput.value = "";
    }
    
    tD5.appendChild(inviteeModPartOfOrgInput);
    inviteeRow.appendChild(tD5);

    let tD6 = document.createElement('td');
    tD6.className = "invitees_list_buttons_cell";
    let inviteeModCommitButton = document.createElement('input');
    inviteeModCommitButton.type = "button";
    inviteeModCommitButton.value = "Valider la modification";
    inviteeModCommitButton.onclick = function() {
        modInvitee(inviteeRow, invitee);
    }
    tD6.appendChild(inviteeModCommitButton);
    let inviteeModCancelButton = document.createElement('input');
    inviteeModCancelButton.type = "button";
    inviteeModCancelButton.value = "Annuler la modification";
    inviteeModCancelButton.onclick = function() {
        updateInviteesTable();
        updatePresenceTable();
    }
    tD6.appendChild(inviteeModCancelButton);

    inviteeRow.appendChild(tD6);
}

function modInvitee(inviteeRow, invitee) {
    // Modification
    let modifiedInviteeLastNames = inviteeRow.children[0].children[0].value; // Il faut bien deux children[] qui se suivent vu que le input est dans une div
    if (modifiedInviteeLastNames != "")
    {
        invitee.lastNames = modifiedInviteeLastNames;
    }

    let modifiedInviteeFirstNames = inviteeRow.children[1].children[0].value;
    if (modifiedInviteeFirstNames != "")
    {
        invitee.firstNames = modifiedInviteeFirstNames;
    }

    let modifiedInviteeGroups = inviteeRow.children[2].children[0].value;
    if (modifiedInviteeGroups != "")
    {
        invitee.groups = [];

        modifiedInviteeGroupsSplitted = modifiedInviteeGroups.split(',');

        modifiedInviteeGroupsSplitted.forEach(group => {
            group = group.trim();
            let casedGroup = "";
            for (let char = 0; char < group.length; char++)
            {
                casedGroup += group[char].toUpperCase();
            }
            // group = group[0].toUpperCase() + group.slice(1); // Pour que toutesles lettres soient en majuscule pour éviter qu'il n'arrive pas bien à trouver ensuite des équivalence.
            invitee.groups.push(casedGroup);
        });
    }

    let modifiedInviteeEmail = inviteeRow.children[3].children[0].value;
    if (modifiedInviteeEmail != ""){
        invitee.email = modifiedInviteeEmail;
    }

    let modifiedInviteePhone = inviteeRow.children[4].children[0].value;
    if (modifiedInviteePhone != ""){
        invitee.phone = modifiedInviteePhone;
    }

    let modifiedInviteePartOfOrg = inviteeRow.children[5].children[0].value;
    if (modifiedInviteePartOfOrg == 'oui' || modifiedInviteePartOfOrg == 'Oui')
    {
        invitee.partOfOrg = true;
    }
    else {
        invitee.partOfOrg = false;
    }

    // Update display
    updateInviteesTable();
    updatePresenceTable();
    computingSpeedDatingInfo();
}

// Supprimer un.e invité.e

function delInvitee(invitee) {
    // Deletion
    let inviteeIndexInInvitees = invitees.indexOf(invitee);
    invitees.splice(inviteeIndexInInvitees, 1); // Il n'y a pas de méthode remove() donc j'utile ce trick en deux étapes. Le deuxième paramètre indique le nombre d'éléments à retirer.

    // Update displays
    updateInviteesTable();
    updatePresenceTable();
    computingSpeedDatingInfo();
}

// Présences des invité.e.s

let presenceTableBody = document.querySelector('#presence_table_body');

function updatePresenceTable() {
    presenceTableBody.innerHTML = "";
    invitees.forEach(invitee => {
        
        let inviteeRow = document.createElement("tr");
        inviteeRow.className = "inner_row";

        let inviteeLastNamesCell = document.createElement('td');
        inviteeLastNamesCell.className = "styled_td";

        if (invitee.lastNames == null){
            inviteeLastNamesCell.textContent = "";
        }
        else{
            inviteeLastNamesCell.textContent = invitee.lastNames;
        }        
        inviteeRow.appendChild(inviteeLastNamesCell);

        let inviteeFirstNamesCell = document.createElement('td');
        inviteeFirstNamesCell.className = "styled_td";

        if (invitee.firstNames == null){
            inviteeFirstNamesCell.textContent = "";
        }
        else{
            inviteeFirstNamesCell.textContent = invitee.firstNames;
        }
        inviteeRow.appendChild(inviteeFirstNamesCell);      

        for (let timePeriodNumbered = 0; timePeriodNumbered < (Object.keys(invitee.presence).length); timePeriodNumbered ++)
        {
            let presenceCell = document.createElement('td');

            let presenceDisplay = document.createElement('p');

            let timePeriodLettered = Object.keys(invitee.presence)[timePeriodNumbered];

            if (invitee.presence[timePeriodLettered] == true){
                presenceDisplay.textContent = "Présent.e";
                presenceCell.style.background = "rgb(31, 199, 134)";
            }
            else 
            {
                presenceDisplay.textContent = "Absent.e";
                presenceCell.style.background = "rgb(209, 87, 87)";
            }
            
            presenceCell.appendChild(presenceDisplay);

            let presenceButton = document.createElement('input');
            presenceButton.type = "button";
            presenceButton.value = 'Changer';
            presenceButton.onclick = function() {presenceSwitch(invitee, timePeriodLettered);
                if (timePeriodLettered == "AM1")
                {
                    computingSpeedDatingInfo();
                }};
            
                

            presenceCell.appendChild(presenceButton);

            inviteeRow.appendChild(presenceCell);
        }

        presenceTableBody.appendChild(inviteeRow);
    });
}

function presenceSwitch(invitee, timePeriodLettered) {
    
    if (invitee.presence[timePeriodLettered] == true)
    {
        invitee.presence[timePeriodLettered] = false;
    }
    else {
        invitee.presence[timePeriodLettered] = true;
    }     
    updatePresenceTable();
}

// Speed dating
// The goal is for people who are not in the same groups to meet

let allGroupsOccurrences = []; // One occurrence is represented in this array as one group
let allGroupsPairedToOccurrences = {};
let allOccurrences = []; // Values of the object above
let allOccurrencesOrdered = [];
let allGroupsOrdered = []; // From least reccurrent to most reccurent if possible

function computingSpeedDatingInfo() {
    
    // Faut commencer par nettoyer les dates pour pas avoir de problème.
    invitees.forEach(invitee => {
        invitee.speedDatesByRound = {
            1: null,
            2: null,
            3: null,
            4: null
        };
    });
    // En fait faut pas commencer par celleux qui ont telle ou telle caractéristique. Il faut écouler les personnes dans l'ordre inverse de leur facilité à trouver quelqu'un. Si cette facilité tombe à zéro, on les mettra alors random avec d'autres.
    for (let round = 1; round <= 4; round ++)
    {
        let un = determineDatingPossibilities();
        let deux = determineDatingOpportunities(un);
        let trois = determineDatingOrder(deux);
        determineDatingMatches(trois, round);
        
        consoleVerifSpeedDating(round);
    }

    
    updateSpeedDatingDisplay();
}

function consoleVerifSpeedDating(round) {
    // Vérif verbose
    invitees.forEach(invitee => {
        if (invitee.presence.AM1 == true && invitee.partOfOrg == false && invitee.speedDatesByRound[round] == null)
        {
            console.log("Verbose : oops, la personne suivante n'a pas de date, au round " + round.toString() + ".");
            console.log(invitee);
        }
    });
}


function determineDatingPossibilities() {
    let datingCandidates = [];
    invitees.forEach(invitee => {
        if (invitee.presence.AM1 == true && invitee.partOfOrg == false)
        {
            datingCandidates.push(invitee);
        }
    });
    
    return datingCandidates;
}

function determineDatingOpportunities(datingCandidates) {
    
    let datingOpportunities = {};

    datingCandidates.forEach(bachelor => {
        let opportunities = 0;
        datingCandidates.forEach(target => {
            if (bachelor != target)
            {
                let sharedGroup = false;
                if (bachelor.groups != null && target.groups != null)
                {
                    bachelor.groups.forEach(bachelorGroup => {
                        target.groups.forEach(targetGroup => {
                            if (bachelorGroup == targetGroup)
                            {
                                sharedGroup = true;
                            }
                        });
                    });
                }
                // Puis
                if (sharedGroup == false)
                {
                    opportunities += 1;
                }
            }
        });

        // Now that we have a bachelor and a total opportunity number, we can put them together in a object (works as a dictionnary somehow)

        

        if (datingOpportunities[opportunities] == undefined)
        {
            datingOpportunities[opportunities] = [];
        }
        datingOpportunities[opportunities].push(bachelor);

    
    });
    return datingOpportunities;
}


function determineDatingOrder(datingOpportunities) {
// D'abord il faut ordonner les opportunités pour pouvoir d'occuper en priorité de celleux qui en ont peu.
    let datingOrder = []; // Ordonnés selon leur quantité d'opportunités de dates, de la plus petite à la plus grande. Notons qu'on va perdre le nombre d'opportunités en chemin, mais ce n'est pas un soucis normalement.
    // Selon la doc MDN :
    // When using numeric keys, the values are returned in the keys' numerical order
    // Object.values({ 100: 'a', 2: 'b', 7: 'c' }) ['b', 'c', 'a']
    // Donc c'est parfait pour nous ici.
    datingOrder = Object.values(datingOpportunities);
    return datingOrder;
}

function determineDatingMatches(datingOrder, round) {
    
    // Idéalement, on match d'abord celleux avec la plus petite chance de trouver avec un.e autre qui a la plus petite chance de trouver, et ensuite on remonte en fonction des chances au fur et à mesure.

    datingOrder.forEach(bachelorRank => {
        bachelorRank.forEach(bachelor => {
            datingOrder.forEach(targetRank => {
                    // Ici c'est autorisé que le rank1 et 2 soit le même, c'est même l'idéal de trouver quelqu'un du même rank. Donc je mets pas de guard. 
                    targetRank.forEach(target => {
                        if (target.speedDatesByRound[round] == null && bachelor.speedDatesByRound[round] == null)
                        {

                        
                            let sharedGroup = false;
                            if (bachelor.groups != null && target.groups != null)
                            {
                                bachelor.groups.forEach(bachelorGroup => {
                                    target.groups.forEach(targetGroup => {
                                        if (bachelorGroup == targetGroup)
                                        {
                                            sharedGroup = true;
                                        }
                                    });
                                });
                            }

                            let alreadyKeen = false;
                            if (bachelor.speedDatesByRound != null && target.speedDatesByRound != null)
                            {
                                for (let targetSpeedDateRound = 1; targetSpeedDateRound <= 4; targetSpeedDateRound++)
                                {
                                    if (target.speedDatesByRound[targetSpeedDateRound] != null && target.speedDatesByRound[targetSpeedDateRound].lastNames != null && target.speedDatesByRound[targetSpeedDateRound].firstNames != null && bachelor.firstNames != null && bachelor.lastNames != null && bachelor.lastNames == target.speedDatesByRound[targetSpeedDateRound].lastNames && bachelor.firstNames == target.speedDatesByRound[targetSpeedDateRound].firstNames)
                                    {
                                        alreadyKeen = true;
                                    } 
                                    if (target.speedDatesByRound[targetSpeedDateRound] != null) {
                                        if (bachelor.lastNames == target.speedDatesByRound[targetSpeedDateRound].lastNames && bachelor.firstNames == target.speedDatesByRound[targetSpeedDateRound].firstNames)
                                        {
                                            alreadyKeen = true;
                                        }
                                    }
                                        
                                    
                                }
                            }


                            let themselves = false;
                            if (bachelor.lastNames == target.lastNames && bachelor.firstNames == target.firstNames)
                            {
                                themselves = true;
                            }

                            // Puis si apres ces verifs il y a tjrs pas de groupe en commun ni d'autres problemes :
                            if (sharedGroup == false && alreadyKeen == false && themselves == false)
                            {

                                let bachelorDateResult = {
                                    "lastNames":null,
                                    "firstNames":null,
                                    "groups":null
                                };
                                if (target.lastNames != null) {
                                    bachelorDateResult.lastNames = target.lastNames.toString();
                                }
                                if (target.firstNames != null)
                                {
                                    bachelorDateResult.firstNames = target.firstNames.toString();
                                }
                                if (target.groups != null)
                                {
                                bachelorDateResult.groups = target.groups.toString();
                                }
                                
                                bachelor.speedDatesByRound[round] = bachelorDateResult;

                                let targetDateResult = {
                                    "lastNames":null,
                                    "firstNames":null,
                                    "groups":null
                                };
                                if (bachelor.lastNames != null) {
                                    targetDateResult.lastNames = bachelor.lastNames.toString();
                                }
                                if (bachelor.firstNames != null)
                                {
                                    targetDateResult.firstNames = bachelor.firstNames.toString();
                                }
                                if (bachelor.groups != null)
                                {
                                    targetDateResult.groups = bachelor.groups.toString();
                                }

                                target.speedDatesByRound[round] = targetDateResult;
                            }
                        }
                    });
                });
            
            
        });
    });

    // Ensuite on effectue un second passage dans lequel on autorise les personnes appartenant à un ou plusieurs groupes en commun de se retrouver.

    datingOrder.forEach(bachelorRank => {
        bachelorRank.forEach(bachelor => {
            if (bachelor.speedDatesByRound[round] == null)
            {
                datingOrder.forEach(targetRank => {
                    targetRank.forEach(target => {
                        if (target.speedDatesByRound[round] == null)
                        {
                            let alreadyKeen = false;
                            if (bachelor.speedDatesByRound != null && target.speedDatesByRound != null)
                            {
                                for (let targetSpeedDateRound = 1; targetSpeedDateRound <= 4; targetSpeedDateRound++)
                                {
                                    if (target.speedDatesByRound[targetSpeedDateRound] != null && target.speedDatesByRound[targetSpeedDateRound].lastNames != null && target.speedDatesByRound[targetSpeedDateRound].firstNames != null && bachelor.firstNames != null && bachelor.lastNames != null && bachelor.lastNames == target.speedDatesByRound[targetSpeedDateRound].lastNames && bachelor.firstNames == target.speedDatesByRound[targetSpeedDateRound].firstNames)
                                    {
                                        alreadyKeen = true;
                                    } 
                                    if (target.speedDatesByRound[targetSpeedDateRound] != null) {
                                        if (bachelor.lastNames == target.speedDatesByRound[targetSpeedDateRound].lastNames && bachelor.firstNames == target.speedDatesByRound[targetSpeedDateRound].firstNames)
                                        {
                                            alreadyKeen = true;
                                        }
                                    }
                                }
                            }

                            let themselves = false;
                            if (bachelor.lastNames == null && target.lastNames == null &&bachelor.firstNames == null && target.firstNames == null)
                            {
                                themselves = true;
                            }
                            else if (bachelor.lastNames == target.lastNames && bachelor.firstNames == target.firstNames)
                            {
                                themselves = true;
                            }
                            else if (bachelor.lastNames == null && target.lastNames == null && bachelor.firstNames == target.firstNames)
                            {
                                themselves = true;
                            }
                            
                            
                            
                            if (alreadyKeen == false && themselves == false)
                            {

                                let bachelorDateResult = {
                                    "lastNames":null,
                                    "firstNames":null,
                                    "groups":null
                                };
                                if (target.lastNames != null) {
                                    bachelorDateResult.lastNames = target.lastNames.toString();
                                }
                                if (target.firstNames != null)
                                {
                                    bachelorDateResult.firstNames = target.firstNames.toString();
                                }
                                if (target.groups != null)
                                {
                                bachelorDateResult.groups = target.groups.toString();
                                } 
    
                                bachelor.speedDatesByRound[round] = bachelorDateResult;
    
                                let targetDateResult = {
                                    "lastNames":null,
                                    "firstNames":null,
                                    "groups":null
                                };
                                if (bachelor.lastNames != null) {
                                    targetDateResult.lastNames = bachelor.lastNames.toString();
                                }
                                if (bachelor.firstNames != null)
                                {
                                    targetDateResult.firstNames = bachelor.firstNames.toString();
                                }
                                if (bachelor.groups != null)
                                {
                                    targetDateResult.groups = bachelor.groups.toString();
                                }
    
                                target.speedDatesByRound[round] = targetDateResult;
                            }
                            
                        }
                    });
                });
            }
            
        });
        
    });

}


function updateSpeedDatingDisplay() {
    let speedDatingResultTable = document.querySelector('#speed_dating_result_table');
    speedDatingResultTable.innerHTML ="";

    invitees.forEach(invitee => {
        // On créée un row par invité.e concerné.e
        if (invitee.partOfOrg != true && invitee.presence.AM1 == true)
        {
            // Créer le row
            let speedDatingResultRow = document.createElement('tr');
            speedDatingResultRow.className = "speed_dating_result_row";

            // Compléter le row
            // Créer, compléter et ajouter les deux colonnes pour "je suis"
            let speedDatingResultCellJeSuis1 = document.createElement('td');
            let speedDatingResultCellJeSuis2 = document.createElement('td');
            speedDatingResultCellJeSuis1.textContent = invitee.lastNames;
            speedDatingResultCellJeSuis1.className = "styled_td_narrow";
            speedDatingResultCellJeSuis2.textContent = invitee.firstNames;
            speedDatingResultCellJeSuis2.className = "styled_td_narrow";
            speedDatingResultRow.appendChild(speedDatingResultCellJeSuis1);
            speedDatingResultRow.appendChild(speedDatingResultCellJeSuis2);
            // Les autres sont dans une boucle for basée sur le nombre de round
            for (let round = 1; round <= 4; round++)
            {
                let speedDatingResultCellRoundSpec1 = document.createElement('td');
                let speedDatingResultCellRoundSpec2 = document.createElement('td');
                let speedDatingResultCellRoundSpec3 = document.createElement('td');

                if (invitee.speedDatesByRound[round] != null)
                {
                    speedDatingResultCellRoundSpec1.textContent = invitee.speedDatesByRound[round].lastNames ;
                    speedDatingResultCellRoundSpec2.textContent = invitee.speedDatesByRound[round].firstNames;
                    speedDatingResultCellRoundSpec3.textContent = invitee.speedDatesByRound[round].groups;
                }
                else {
                speedDatingResultCellRoundSpec1.textContent = "";
                speedDatingResultCellRoundSpec2.textContent = "";
                speedDatingResultCellRoundSpec3.textContent = "";
                }
                speedDatingResultCellRoundSpec1.className = "styled_td_narrow";
                speedDatingResultCellRoundSpec2.className = "styled_td_narrow";
                speedDatingResultCellRoundSpec3.className = "styled_td_narrow";
                
                speedDatingResultRow.appendChild(speedDatingResultCellRoundSpec1);
                speedDatingResultRow.appendChild(                speedDatingResultCellRoundSpec2);
                speedDatingResultRow.appendChild(speedDatingResultCellRoundSpec3);
            }

            // Ajouter le row
            speedDatingResultTable.appendChild(speedDatingResultRow);
            
        }
    });
}